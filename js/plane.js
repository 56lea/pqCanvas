function planeGift(id){
     var $this = this;
     var plane = new Image();
     var cloud = new Image();
     var heart = new Image();
     var arrow = new Image();
     var cxt = null;
     var t = 0;
     this.id = id;
     var w=$(window).width();
     var h=$(window).height();
     var loadSuccessNum=0;
     var imgNum=4;
     var p={x:240,y:230};
     var pq=new zTools();
     this.init =function(){
        var canvas=document.getElementById(this.id);
        canvas.width=w;
        canvas.height=h;
         pq.init(canvas);
        cxt =canvas.getContext('2d');
        cxt.imageSmoothingEnabled=true;
        
        plane.src="img/air/plane.png";
        plane.onload=function(){$this.onLoaded();}
        cloud.src="img/air/cloud.png";
        cloud.onload=function(){$this.onLoaded();}
        heart.src="img/air/heart.png";
        heart.onload=function(){$this.onLoaded();}
        arrow.src="img/air/arrow.png";
        arrow.onload=function(){$this.onLoaded();}
     };
     this.onLoaded=function(){
         loadSuccessNum++;
         if(loadSuccessNum==imgNum){
             $this.run();
         }
     }
     this.run = function(){
         pq.to({img:plane,x:w+plane.width,y:0,scaleX:-1,rotation:-15},0.8,{rotation:-15,x:0-plane.width,y:h*2/3,autoRemove:true});
         
         var obj={img:cloud,x:w/2,y:h/2,width:1,height:1};
         var param={delay:0.4,x:w/2-cloud.width/2,y:h/2-cloud.height/2,width:cloud.width,height:cloud.height,autoRemove:true};
         pq.to(obj,1,param);
         
         obj={img:cloud,x:w/2-cloud.width/2,y:h/2-cloud.height/2,width:cloud.width,height:cloud.height};
         param={delay:1.4,x:w/2-cloud.width/2+10,y:h/2-cloud.height/2+10,width:cloud.width-20,height:cloud.height-20,autoRemove:true};
         pq.to(obj,1,param);
         
         obj={img:cloud,x:w/2-cloud.width/2+10,y:h/2-cloud.height/2+10,width:cloud.width-20,height:cloud.height-20};
         param={delay:2.4,x:w/2-cloud.width/2+10,y:h/2-cloud.height/2+10,autoRemove:true,onComplete:$this.theEnd};
         pq.to(obj,4,param);
         
         pq.to({img:arrow,x:0-arrow.width,y:h*2/3},1,{delay:1,x:w+arrow.width,y:-100});
         pq.to({img:plane,x:0-plane.width,y:0-plane.height,rotation:15},5,{delay:1.4,rotation:15,x:w+plane.width,y:h/2,autoRemove:true});
         for(var i=0;i<100;i++){
             pq.to({img:heart,x:w/2+Math.random()*50-25,y:h/2+Math.random()*50-25,alpha:0},2,{delay:Math.random()*3+1.5,alpha:1,x:Math.random()*800-200,y:0-heart.height});
         }
     };
     this.theEnd=function(){
        pq.clear();
        //动画结束
     }
 }
 
 $(document).ready(function(){
    var g = new planeGift('canvas');
    g.init();
});