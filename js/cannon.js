function cannonGift(id){
     var $this = this;
     var hl = new Image();
     var dp = new Image();
     var lz = new Image();
     var hb = new Image();
     var pd = new Image();
     var dl = new Image();
     var cxt = null;
     var t = 0;
     this.id = id;
     var w=$(window).width();
     var h=$(window).height();
     var loadSuccessNum=0;
     var imgNum=6;
     var p={x:-200,y:300};
     var p2={x:10,y:300};
     var pq=new zTools();
     this.init =function(){
        var canvas=document.getElementById(this.id);
        canvas.width=w;
        canvas.height=h;
        console.log(w+"--"+h);
        pq.init(canvas);
        cxt =canvas.getContext('2d');
        cxt.imageSmoothingEnabled=true;
        
        hl.src="img/cannon/hl.png";
        hl.onload=function(){$this.onLoaded();}
        lz.src="img/cannon/lz.png";
        lz.onload=function(){$this.onLoaded();}
        dp.src="img/cannon/dp.png";
        dp.onload=function(){$this.onLoaded();}
        hb.src="img/cannon/hb.png";
        hb.onload=function(){$this.onLoaded();}
        pd.src="img/cannon/pd.png";
        pd.onload=function(){$this.onLoaded();}
        dl.src="img/cannon/dl.png";
        dl.onload=function(){$this.onLoaded();}
     };
     this.onLoaded=function(){
         loadSuccessNum++;
         if(loadSuccessNum==imgNum){
             $this.run();
         }
     }
     this.run = function(){
         pq.to({img:hl,x:p.x,y:p.y},1,{x:p2.x,y:p2.y,autoRemove:true});
         pq.to({img:lz,x:p.x+80,y:p.y+50},1,{x:p2.x+80,y:p2.y+50,autoRemove:true});
         pq.to({img:dp,x:p.x+55,y:p.y+10,rotation:15},1,{rotation:15,x:p2.x+50,y:p2.y,autoRemove:true});
         pq.to({img:hb,x:p.x+50,y:p.y+50},1,{x:p2.x+50,y:p2.y+50,autoRemove:true});
         
         pq.to({img:hl,x:p2.x,y:p2.y},7,{delay:1,x:p2.x,y:p2.y,autoRemove:true});
         pq.to({img:lz,x:p2.x+80,y:p2.y+50},7,{delay:1,x:p2.x+80,y:p2.y+50,autoRemove:true});
         pq.to({img:dp,x:p2.x+55,y:p2.y+10},7,{delay:1,x:p2.x+50,y:p2.y,autoRemove:true});
         
         pq.to({img:hb,x:p2.x+50,y:p2.y+50,rotation:30},1,{rotation:45,delay:1,x:p2.x+70,y:p2.y+50,autoRemove:true});
         pq.to({img:hb,x:p2.x+70,y:p2.y+50,rotation:45},1,{delay:2,rotation:30,x:p2.x+70,y:p2.y+50,autoRemove:true});
         pq.to({img:hb,x:p2.x+70,y:p2.y+50,rotation:30},5,{delay:3,rotation:30,x:p2.x+70,y:p2.y+50,autoRemove:true});
         
         pq.to({img:pd,x:p2.x+150,y:p2.y},0.5,{delay:2,x:w-pd.width,y:h/5,autoRemove:true});
         pq.to({img:pd,x:w-pd.width,y:h/5},0.5,{delay:2.5,x:w*2/3,y:50,autoRemove:true});
         pq.to({img:pd,x:w*2/3,y:50,width:pd.width,height:pd.height,alpha:1},0.5,{delay:3,alpha:0,width:pd.width+60,height:pd.height+60,x:w*2/3-25,y:25,autoRemove:true});
         
         pq.to({img:dl,x:w*2/3,y:50,rotation:15},1,{delay:3.5,rotation:-15,x:w*2/3,y:50,autoRemove:true});
         pq.to({img:dl,x:w*2/3,y:50,rotation:-15},1,{delay:4.5,rotation:15,x:w*2/3,y:50,autoRemove:true});
         pq.to({img:dl,x:w*2/3,y:50,rotation:15},1,{delay:5.5,rotation:-15,x:w*2/3,y:50,autoRemove:true});
         pq.to({img:dl,x:w*2/3,y:50,rotation:-15},1,{delay:6.5,rotation:15,x:w*2/3,y:50,autoRemove:true,onComplete:$this.theEnd});
         //pq.to({img:dl,x:w*2/3,y:50,rotation:15},1,{delay:7.5,rotation:-15,x:w*2/3,y:50,autoRemove:true});
         //pq.to({img:dl,x:w*2/3,y:50,rotation:-15},1,{delay:8.5,rotation:15,x:w*2/3,y:50,autoRemove:true});
     };
     this.theEnd=function(){
         pq.clear();
         //动画结束
     }
 }
 
 $(document).ready(function(){
    var g = new cannonGift('canvas');
    g.init();
});